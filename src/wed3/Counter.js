import React, { Component } from 'react';

class Counter extends Component {

    state = {
        counter: 0
    }

    render() {
        return (
            <div>
                {this.state.counter}
                <button onClick={this.increment.bind(this)}>Increment</button>
            </div>
        );
    }

    increment(){
        this.setState( state => ({counter: this.state.counter + 1}));
    }
}

export default Counter;