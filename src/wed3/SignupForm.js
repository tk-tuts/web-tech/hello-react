import React from 'react';

class SignupForm extends React.Component {

    state = {
        username: '',
        password: ''
    };

    render = () =>
        <form onSubmit={this.handleSubmit}>
            <div>
                <label>Username</label>
                <input value={this.state.username} onChange={this.handleUsernameChange} type="text" />
            </div>
            <div>
                <label>Password</label>
                <input value={this.state.password} onChange={this.handlePasswordChange} type="password" />
            </div>
            <div>
                <button type="submit">Login</button>
            </div>
        </form>
    
    handleUsernameChange = (event) => {
        this.setState({
            username: event.target.value
        });
    }

    handlePasswordChange = (event) => {
        this.setState({
            password: event.target.value
        });
    }

    handleSubmit = (event) => {        
        event.preventDefault();
    }

}

export default SignupForm;